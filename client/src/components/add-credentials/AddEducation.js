import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import TextFieldGroup from "../common/textFieldGroup.js";
import TextAreaFieldGroup from "../common/textAreaFieldGroup";
import { addEducation } from '../../actions/profileAction';

class AddEducation extends Component {
  state = {
    school: "",
    degree: "",
    fieldofstudy: "",
    fromdate: "",
    todate: "",
    current: false,
    description: "",
    errors: {},
    disabled: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.errors) {
      return ({ errors: nextProps.errors }) // <- this is setState equivalent
    }
  }

  handleCheck(evt) {
    this.setState({
        disabled: !this.state.disabled,
        current: !this.state.current
    })
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }
  handleSubmit = (evt) => {
    evt.preventDefault();
    const expData = {
        school: this.state.school,
        degree: this.state.degree,
        fieldOfStudy: this.state.fieldofstudy,
        fromdate: this.state.fromdate,
        todate: this.state.todate,
        current: this.state.current,
        description: this.state.description,
    };
    this.props.onaddEducation(expData, this.props.history)
  };


  render() {
    const { errors } = this.state;
    return (
      <div className="add-experience">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="text-center display-4">Add Experience</h1>
              <p className="lead text-center">
                Add education qualification, degree, bootcamp you have attended.
              </p>
              <small className="d-block pb-3">* = Required Field</small>
              <form noValidate onSubmit={(evt) => this.handleSubmit(evt)}>
                <TextFieldGroup
                  placeholder="* School"
                  name="school"
                  value={this.state.school}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.school}
                  info="This could be your school where you studied"
                />
                <TextFieldGroup
                  placeholder="* Degree or Certification"
                  name="degree"
                  value={this.state.degree}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.degree}
                  info="This is the degree you have completed."
                />
                <TextFieldGroup
                  placeholder="* Field of Study"
                  name="fieldofstudy"
                  value={this.state.fieldofstudy}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.fieldOfStudy}
                  info="This could be the degree/program you have attended"
                />
                <h6>From Date</h6>
                <TextFieldGroup
                  type="date"
                  placeholder="* From Date"
                  name="fromdate"
                  value={this.state.fromdate}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.fromdate}
                />
                <h6>To Date</h6>
                <TextFieldGroup
                  type="date"
                  placeholder="To Date"
                  name="todate"
                  value={this.state.todate}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.todate}
                  disabled={this.state.disabled ? 'disabled' : ''}
                />
                <div className="form-check mb-4">
                    <input 
                    type="checkbox" 
                    className="form-check-input"
                    name="current"
                    value={this.state.current}
                    checked={this.state.current}
                    onChange={(evt) => this.handleCheck(evt)}
                    id="current"
                    />
                    <label htmlFor="current" className="form-check-label">
                        Current Job
                    </label>
                </div>
                <TextAreaFieldGroup
                  placeholder="Program Description"
                  name="description"
                  value={this.state.description}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.description}
                  info="Tell us about the program you have attended"
                />
                <input type="submit" value="Submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddEducation.propTypes = {
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  errors: state.errors,
});

const mapDispatchToProps = (dispatch) => {
  return {
      onaddEducation: (data, history) => dispatch(addEducation(data, history))
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AddEducation));
