const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
    let errors = {};

    data.name = !isEmpty(data.name) ? data.name : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.password2 = !isEmpty(data.password2) ? data.password2 : '';

    if(!validator.isLength(data.name, {min: 3, max: 30})) {
        errors.name = 'Name must be between 3 and 30 character';
    }

    if(validator.isEmpty(data.name)) {
        errors.name = 'Name field is required';
    }

    if(validator.isEmpty(data.email)) {
        errors.email = 'Email field is required';
    }

    if(validator.isEmpty(data.password)) {
        errors.password = 'Password field is required';
    }

    if(validator.isEmpty(data.password2)) {
        errors.password2 = 'Password2 field is required';
    }

    if(!validator.isEmpty(data.password) && !validator.isEmpty(data.password2)) {
        if(data.password !== data.password2) {
            errors.password2 = 'Password and Password2 did not match';
        }
    }

    if(!validator.isEmail(data.email)) {
        errors.email = 'Provide valid email address';
    }

    if(!validator.isLength(data.password, {min: 6, max: 30})) {
        errors.password = 'Password must be between 6 and 30 character';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};