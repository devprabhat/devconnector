import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import TextFieldGroup from "../common/textFieldGroup.js";
import TextAreaFieldGroup from "../common/textAreaFieldGroup";
import { addExperience } from '../../actions/profileAction';

class AddExperience extends Component {
  state = {
    company: "",
    title: "",
    location: "",
    fromdate: "",
    todate: "",
    current: false,
    description: "",
    errors: {},
    disabled: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.errors) {
      return ({ errors: nextProps.errors }) // <- this is setState equivalent
    }
  }

  handleCheck(evt) {
    this.setState({
        disabled: !this.state.disabled,
        current: !this.state.current
    })
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }
  handleSubmit = (evt) => {
    evt.preventDefault();
    const expData = {
        company: this.state.company,
        title: this.state.title,
        location: this.state.location,
        fromdate: this.state.fromdate,
        todate: this.state.todate,
        current: this.state.current,
        description: this.state.description,
    };
    this.props.onaddExperience(expData, this.props.history)
  };


  render() {
    const { errors } = this.state;
    return (
      <div className="add-experience">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="text-center display-4">Add Experience</h1>
              <p className="lead text-center">
                Add any job or position that you have had in the past or current
              </p>
              <small className="d-block pb-3">* = Required Field</small>
              <form noValidate onSubmit={(evt) => this.handleSubmit(evt)}>
                <TextFieldGroup
                  placeholder="Company"
                  name="company"
                  value={this.state.company}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.company}
                  info="This could be your own company or one you work for"
                />
                <TextFieldGroup
                  placeholder="Title"
                  name="title"
                  value={this.state.title}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.title}
                  info="This could be your own company or one you work for"
                />
                <TextFieldGroup
                  placeholder="Location"
                  name="location"
                  value={this.state.location}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.location}
                  info="This could be your own company or one you work for"
                />
                <h6>From Date</h6>
                <TextFieldGroup
                  type="date"
                  placeholder="From Date"
                  name="fromdate"
                  value={this.state.fromdate}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.fromdate}
                />
                <h6>To Date</h6>
                <TextFieldGroup
                  type="date"
                  placeholder="To Date"
                  name="todate"
                  value={this.state.todate}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.todate}
                  disabled={this.state.disabled ? 'disabled' : ''}
                />
                <div className="form-check mb-4">
                    <input 
                    type="checkbox" 
                    className="form-check-input"
                    name="current"
                    value={this.state.current}
                    checked={this.state.current}
                    onChange={(evt) => this.handleCheck(evt)}
                    id="current"
                    />
                    <label htmlFor="current" className="form-check-label">
                        Current Job
                    </label>
                </div>
                <TextAreaFieldGroup
                  placeholder="Description"
                  name="description"
                  value={this.state.description}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.description}
                  info="Tell us a little info about this job"
                />
                <input type="submit" value="Submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddExperience.propTypes = {
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  errors: state.errors,
});

const mapDispatchToProps = (dispatch) => {
  return {
      onaddExperience: (data, history) => dispatch(addExperience(data, history))
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AddExperience));
