import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TextFieldGroup from "../common/textFieldGroup";
import TextAreaFieldGroup from "../common/textAreaFieldGroup";
import InputGroup from "../common/inputGroup";
import SelectListGroup from "../common/selectListGroup";
import { createProfile } from '../../actions/profileAction';
import { withRouter } from 'react-router-dom';

class CreateProfile extends Component {
  state = {
    dispalaySocialImput: false,
    handle: "",
    company: "",
    website: "",
    location: "",
    status: "",
    skills: "",
    github: "",
    bio: "",
    twitter: "",
    facebook: "",
    linkedin: "",
    youtube: "",
    errors: {},
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.errors) {
      return ({ errors: nextProps.errors }) // <- this is setState equivalent
    }
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }
  handleSubmit = (evt) => {
    evt.preventDefault();
    this.props.onCreatePropfile(this.state, this.props.history)
  };

  render() {
    const { errors, dispalaySocialImput } = this.state;
    //Select option for status
    const options = [
      {
        label: "* Select Professional status",
        value: 0,
      },
      { label: "Developer", value: "Developer" },
      { label: "Senior Developer", value: "Senior Developer" },
      { label: "Manager", value: "Manager" },
      { label: "Student", value: "Student" },
      { label: "Intern", value: "Intern" },
      { label: "Others", value: "Others" },
    ];

    let socialInputs;
    if(dispalaySocialImput) {
        socialInputs = (
            <div>
                <InputGroup 
                    placeholder="Twitter Profile URL"
                    name="twitter"
                    icon="fab fa-twitter"
                    value={this.state.twitter}
                    onchange={(evt) => this.handleChange(evt)}
                    error={errors.twitter}
                />
                <InputGroup 
                    placeholder="Facebook Profile URL"
                    name="facebook"
                    icon="fab fa-facebook"
                    value={this.state.facebook}
                    onchange={(evt) => this.handleChange(evt)}
                    error={errors.facebook}
                />
                <InputGroup 
                    placeholder="LinkedIn Profile URL"
                    name="linkedin"
                    icon="fab fa-linkedin"
                    value={this.state.linkedin}
                    onchange={(evt) => this.handleChange(evt)}
                    error={errors.linkedin}
                />
                <InputGroup 
                    placeholder="Youtube Profile URL"
                    name="youtube"
                    icon="fab fa-youtube"
                    value={this.state.youtube}
                    onchange={(evt) => this.handleChange(evt)}
                    error={errors.youtube}
                />
            </div>
        )
    }

    return (
      <div className="craete-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Create your profile</h1>
              <p className="lead text-center">
                Let's get some information to make your profile standout.
              </p>
              <small className="d-block">* = required field</small>
              <form noValidate onSubmit={(evt) => this.handleSubmit(evt)}>
                <TextFieldGroup
                  placeholder="* Profile Handle"
                  name="handle"
                  value={this.state.handle}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.handle}
                  info="A unique handle for your profile url. Your full name, company name, nickname etc.(This can't be changed after)"
                />
                <SelectListGroup
                  placeholder="* Status"
                  name="status"
                  value={this.state.status}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.status}
                  options={options}
                  info="A unique handle Give an idea where you are in your career."
                />
                <TextFieldGroup
                  placeholder="Company"
                  name="company"
                  value={this.state.company}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.company}
                  info="This could be your own company or one you work for"
                />
                <TextFieldGroup
                  placeholder="Website"
                  name="website"
                  value={this.state.website}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.website}
                  info="This could be your own website"
                />
                <TextFieldGroup
                  placeholder="Location"
                  name="location"
                  value={this.state.location}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.location}
                  info="City or city and state (eg. Kolkata, West Bengal)"
                />
                <TextFieldGroup
                  placeholder="Skills"
                  name="skills"
                  value={this.state.skills}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.skills}
                  info="Please use comma separated values(HTML, CSS, JavaScript, ReactJS)"
                />
                <TextFieldGroup
                  placeholder="Github username"
                  name="github"
                  value={this.state.github}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.github}
                  info="Please provide your github username"
                />
                <TextAreaFieldGroup
                  placeholder="Short Bio"
                  name="bio"
                  value={this.state.bio}
                  onchange={(evt) => this.handleChange(evt)}
                  error={errors.bio}
                  info="Tell us a little info about yourself"
                />
                <div className="mb-3">
                    <button type="button" onClick={() => {this.setState(preState => ({dispalaySocialImput: !preState.dispalaySocialImput}))}} className="btn btn-light">
                        Add Social network here
                    </button>
                    <span className="test-muted mr-4">Optional</span>
                </div>
                { socialInputs }
                <input type="submit" value="Submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CreateProfile.propTypes = {
  //createPropfile: PropTypes.func,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  errors: state.errors,
});

const mapDispatchToProps = dispatch => {
    return {
        onCreatePropfile: (data, history) => dispatch(createProfile(data, history))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CreateProfile));
