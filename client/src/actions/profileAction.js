import {
  GET_PROFILE,
  GET_PROFILES,
  PROFILE_LOADING,
  CLEAR_CURRENT_PROFILE,
  GET_ERRORS,
  SET_CURRENT_USER,
} from "./actionTypes";
import axios from "axios";

//GET current profiles
export const getCurrentProfile = () => {
  return (dispatch) => {
    dispatch(setProfileLoading());
    axios
      .get("/api/profile")
      .then((response) => {
        dispatch({
          type: GET_PROFILE,
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_PROFILE,
          payload: {},
        });
      });
  };
};

//Create Profile
export const createProfile = (profileData, history) => {
  return (dispatch) => {
    dispatch(setProfileLoading());
    axios
      .post("/api/profile", profileData)
      .then((res) => history.push("/dashboard"))
      .catch((err) => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data,
        });
      });
  };
};

//Delete Account

export const deleteAccount = () => {
  return (dispatch) => {
    if (
      window.confirm("Are you sure you want to delete? This can not be undone.")
    ) {
      axios
        .delete("/api/profile")
        .then((res) => {
          dispatch({
            type: SET_CURRENT_USER,
            payload: {},
          });
          dispatch(clearCurrentProfile())
        })
        .catch((err) =>
          dispatch({
            type: GET_ERRORS,
            payload: err.response.data,
          })
        );
    }
  };
};

//Add Experience
export const addExperience = (expData, history) => {
  return (dispatch) => {
    axios.post('/api/profile/experience', expData)
      .then(res => history.push('/dashboard'))
      .catch((err) =>
          dispatch({
            type: GET_ERRORS,
            payload: err.response.data,
          })
        );
  }
}

//Add Experience
export const addEducation = (eduData, history) => {
  return (dispatch) => {
    axios.post('/api/profile/education', eduData)
      .then(res => history.push('/dashboard'))
      .catch((err) =>
          dispatch({
            type: GET_ERRORS,
            payload: err.response.data,
          })
        );
  }
}

//Delete Experience
export const deleteExperience = (expId) => {
  return (dispatch) => {
    dispatch(setProfileLoading());
    axios.delete('/api/profile/experience/'+expId)
      .then(response => {
        dispatch({
          type: GET_PROFILE,
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data,
        })
      })
  }
}

//Delete Education
export const deleteEducation = (expId) => {
  return (dispatch) => {
    dispatch(setProfileLoading());
    axios.delete('/api/profile/education/'+expId)
      .then(response => {
        dispatch({
          type: GET_PROFILE,
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data,
        })
      })
  }
}

//Get all Profiles
export const getProfiles = () => {
  return (dispatch) => {
    dispatch(setProfileLoading());
    axios.get('/api/profile/all')
      .then(response => {
        dispatch({
          type: GET_PROFILES,
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_PROFILES,
          payload: null,
        });
      })
  }
}

//Get Profile by Handle
export const getProfileByHandle = (handle) => {
  return (dispatch) => {
    dispatch(setProfileLoading());
    axios.get(`/api/profile/handle/${handle}`)
      .then(response => {
        dispatch({
          type: GET_PROFILE,
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({
          type: GET_PROFILE,
          payload: {},
        });
      })
  }
}

//Profile loading
export const setProfileLoading = () => {
  return {
    type: PROFILE_LOADING,
  };
};

//Clear profile
export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE,
  };
};
