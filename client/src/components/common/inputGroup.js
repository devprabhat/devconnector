import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const InputGroup = (props) => {
  return (
    <div className="imput-group mb-3">
        <div className="input-group-prepend">
            <span className="input-group-text">
                <i className={props.icon}></i>
            </span>
        </div>
      <input
        className={classnames("form-control form-control-lg", {
          "is-invalid": props.error,
        })}
        placeholder={props.placeholder}
        name={props.name}
        value={props.value}
        required
        onChange={props.onchange}
      />
      {props.error && <div className="invalid-feedback">{props.error}</div>}
    </div>
  );
};

InputGroup.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    error: PropTypes.string,
    onchange: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    icon: PropTypes.string
}

InputGroup.defaultProps = {
    type: 'text',
}


export default InputGroup;
