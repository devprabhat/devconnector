import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getCurrentProfile, deleteAccount } from "../../actions/profileAction";
import Spinner from "../common/Spinner";
import { Link } from "react-router-dom";
import ProfileActions from "./ProfileActions";
import Experience from './Experience';
import Education from './Education';

class Dashboard extends Component {
  componentDidMount() {
    this.props.onProfileLoad();
  }

  onDeleteClick = (evt) => {
    evt.preventDefault();
    this.props.deleteAccount();
  }
  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;

    let dashboardContent;
    if (profile === null || loading === true) {
      dashboardContent = <Spinner />;
    } else {
      //Check if loggedin user has profile data
      if (Object.keys(profile).length > 0) {
        dashboardContent = (
          <div>
            <p className="lead text-muted">Welcome <Link to={`/profile/${profile.handle}`}> {user.name}</Link></p>
            <ProfileActions />
            <Experience experience={profile.experience}/>
            <Education education={profile.education} />
            {/* TODO: Add Experience and Education */}
            <div style={{marginBottom: '16px'}}>
                <button onClick={(evt) => this.onDeleteClick(evt)} className="btn btn-danger">Delete Account</button>
            </div>
          </div>
        );
      } else {
        //User is logged in but no profile
        dashboardContent = (
          <div className="">
            <p className="lead text-muted">Welcome {user.name}</p>
            <p>
              You have not set up your profile yet, please add some information
            </p>
            <Link to="/create-profile" className="btn btn-lg btn-info">
              Create Profile
            </Link>
          </div>
        );
      }
    }
    return (
      <div className="dashboard">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="display-4">Dashboard</h1>
              {dashboardContent}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  getCurrentProfile: PropTypes.func,
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth,
});
const mapDispatchToProps = (dispatch) => {
  return {
    onProfileLoad: () => dispatch(getCurrentProfile()),
    deleteAccount: () => dispatch(deleteAccount())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
