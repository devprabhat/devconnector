import React, { Component } from "react";
import { loginUser } from '../../actions/authAction';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import TextFieldGroup from '../common/textFieldGroup';

class Login extends Component {
  state = {
    email: "",
    password: "",
    errors: {}
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.errors) {
      return ({ errors: nextProps.errors }) // <- this is setState equivalent
    }
  }

  componentDidMount() {
    if(this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  handleSubmit(evt) {
    evt.preventDefault();

    const loginData = {
      email: this.state.email,
      password: this.state.password,
    };
    
    this.props.onLoginUser(loginData, this.props.history);
  }
  render() {
    const { errors } = this.state;
    
    return (
      <div className="container">
        <div className="login">
          <div className="container">
            <div className="row">
              <div className="col-md-8 m-auto">
                <h1 className="display-4 text-center">Log In</h1>
                <p className="lead text-center">
                  Sign in to your DevConnector account
                </p>
                <form noValidate onSubmit={(evt) => this.handleSubmit(evt)}>
                  {/* <div className="form-group">
                    <input
                      type="email"
                      className={classnames("form-control form-control-lg", {
                        "is-invalid" : errors.email
                      })}
                      placeholder="Email Address"
                      name="email"
                      value={this.state.email}
                      required
                      onChange={(evt) => this.handleChange(evt)}
                    />
                    {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
                  </div> */}
                  <TextFieldGroup 
                    name = "email"
                    placeholder = "Email Address"
                    value = {this.state.email}
                    error = {errors.email}
                    type = "email"
                    onchange = {(evt) => this.handleChange(evt)}
                  />
                  {/* <div className="form-group">
                    <input
                      type="password"
                      className={classnames("form-control form-control-lg", {
                        "is-invalid" : errors.password
                      })}
                      placeholder="Password"
                      name="password"
                      value={this.state.password}
                      required
                      onChange={(evt) => this.handleChange(evt)}
                    />
                    {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
                  </div> */}
                  <TextFieldGroup 
                    name = "password"
                    placeholder = "Paasword"
                    value = {this.state.password}
                    error = {errors.password}
                    type = "password"
                    onchange = {(evt) => this.handleChange(evt)}
                  />
                  <input
                    type="submit"
                    className="btn btn-info btn-block mt-4"
                  />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
}
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
})                

const mapDispatchToProps = (dispatch) => {
  return {
    onLoginUser: (data, history) => dispatch(loginUser(data, history)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
