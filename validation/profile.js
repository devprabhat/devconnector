const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfileInput(data) {
    let errors = {};

    data.handle = !isEmpty(data.handle) ? data.handle : '';
    data.status = !isEmpty(data.status) ? data.status : '';
    data.skills = !isEmpty(data.skills) ? data.skills : '';


    if(!validator.isLength(data.handle, {min: 4, max: 24})) {
        errors.handle = 'handle must be between 4 and 24 characters';
    }

    if(validator.isEmpty(data.handle)) {
        errors.handle = 'Profile handle is required';
    }

    if(validator.isEmpty(data.status)) {
        errors.status = 'Profile status is required';
    }

    if(validator.isEmpty(data.skills)) {
        errors.skills = 'Profile skills is required';
    }

    if(!isEmpty(data.website)) {
        if(!validator.isURL(data.website)) {
            errors.website = 'Website url is invalid';
        }
    }

    if(!isEmpty(data.youtube)) {
        if(!validator.isURL(data.youtube)) {
            errors.youtube = 'Youtube url is invalid';
        }
    }

    if(!isEmpty(data.twitter)) {
        if(!validator.isURL(data.twitter)) {
            errors.twitter = 'Twitter url is invalid';
        }
    }

    if(!isEmpty(data.facebook)) {
        if(!validator.isURL(data.facebook)) {
            errors.facebook = 'Facebook url is invalid';
        }
    }

    if(!isEmpty(data.linkedIn)) {
        if(!validator.isURL(data.linkedIn)) {
            errors.linkedIn = 'LinkedIn url is invalid';
        }
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};