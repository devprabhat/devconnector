import React, { Component } from "react";
import { connect } from "react-redux";
import Moment from "react-moment";
import { deleteExperience } from "../../actions/profileAction";

class Experience extends Component {
  deleteExperience = (id) => {
    this.props.onDeleteExperience(id);
  };
  render() {
    const experience = this.props.experience.map((exp) => (
      <tr key={exp._id}>
        <td>{exp.company}</td>
        <td>{exp.title}</td>
        <td>{exp.location}</td>
        <td>
          <Moment format="DD/MM/YYYY">{exp.fromdate}</Moment> -
          {exp.todate === null ? (
            " Now"
          ) : (
            <Moment format="DD/MM/YYYY">{exp.todate}</Moment>
          )}
        </td>
        <td>
          <button
            className="btn btn-danger"
            onClick={() => this.deleteExperience(exp._id)}
          >
            Delete
          </button>
        </td>
      </tr>
    ));
    return (
      <div className="mb-2">
        <h4 className="mb-4">Experience Credentials</h4>
        <table className="table">
          <thead>
            <tr>
              <th>Company</th>
              <th>Title</th>
              <th>Location</th>
              <th>Years</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{experience}</tbody>
        </table>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      onDeleteExperience: (expId) =>
      dispatch(deleteExperience(expId)),
  };
};
export default connect(null, mapDispatchToProps)(Experience);
