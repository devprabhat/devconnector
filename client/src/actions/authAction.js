import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

import { GET_ERRORS, SET_CURRENT_USER } from "./actionTypes";

//Register User
export const registerUser = (userData, history) => {
  return (dispatch) => {
    axios
      .post("/api/users/register", userData)
      .then((res) => history.push("/login"))
      .catch((err) =>
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data,
        })
      );
  };
};

//Login User - Get User token
export const loginUser = (userData, history) => {
  return (dispatch) => {
    axios
      .post("/api/users/login", userData)
      .then((res) => {
        //Save to local storage
        const { token } = res.data;
        //Set local storage
        localStorage.setItem("jwtToken", token);
        //Set token to auth headers
        setAuthToken(token);
        //Decode token to get user data
        const decode = jwt_decode(token);
        //Set current user
        dispatch(setCurrentUser(decode));
        history.push('/dashboard')
      })
      .catch((err) => {
          console.log('Hello');
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data,
        });
      });
  };
};

//Set logged in user
export const setCurrentUser = (decode) => {
  return {
    type: SET_CURRENT_USER,
    payload: decode,
  };
};

//Logout user
export const logoutUser = () => {
  return dispatch => {
    //Remove token from localStorage
    localStorage.removeItem('jwtToken');
    //Remove auth header for future
    setAuthToken(false);
    //Set current user to {} which will set isAuthenticated false
    dispatch(setCurrentUser({}));
    //Redirect to login
    window.location.href = '/login';
  }
}
