const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateExperienceInput(data) {
    let errors = {};

    data.title = !isEmpty(data.title) ? data.title : '';
    data.company = !isEmpty(data.company) ? data.company : '';
    data.fromdate = !isEmpty(data.fromdate) ? data.fromdate : '';

    if(validator.isEmpty(data.title)) {
        errors.title = 'Title is required';
    }
    
    if(validator.isEmpty(data.company)) {
        errors.company = 'Company is required';
    }

    if(validator.isEmpty(data.fromdate)) {
        errors.fromdate = 'From date is required';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};