import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/authAction';
import TextFieldGroup from '../common/textFieldGroup';

class Register extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    password2: "",
    errors: {},
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.errors) {
      return ({ errors: nextProps.errors }) // <- this is setState equivalent
    }
  }

  componentDidMount() {
    if(this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  handleSubmit(evt) {
    evt.preventDefault();
    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
    };

    this.props.onRegisterUser(newUser, this.props.history);
  }

  render() {

    const { errors } = this.state;
    return (
      <div className="container">
        <div className="register">
          <div className="container">
            <div className="row">
              <div className="col-md-8 m-auto">
                <h1 className="display-4 text-center">Sign Up</h1>
                <p className="lead text-center">
                  Create your DevConnector account
                </p>
                <form noValidate onSubmit={(evt) => this.handleSubmit(evt)}>
                  <TextFieldGroup 
                    name = "name"
                    placeholder = "Name"
                    value = {this.state.name}
                    error = {errors.name}
                    type = "email"
                    onchange = {(evt) => this.handleChange(evt)}
                  />
                  
                  <TextFieldGroup 
                    name = "email"
                    placeholder = "Email Address"
                    value = {this.state.email}
                    error = {errors.email}
                    type = "email"
                    info = {`This site uses Gravatar so if you want a profile image,
                    use a Gravatar email`}
                    onchange = {(evt) => this.handleChange(evt)}
                  />

                  <TextFieldGroup 
                    name = "password"
                    placeholder = "Paasword"
                    value = {this.state.password}
                    error = {errors.password}
                    type = "password"
                    onchange = {this.handleChange.bind(this)}
                  />
                  
                  <TextFieldGroup 
                    name = "password2"
                    placeholder = "Confirm Password"
                    value = {this.state.password2}
                    error = {errors.password2}
                    type = "password"
                    onchange = {this.handleChange.bind(this)}
                  />
                  <input
                    type="submit"
                    className="btn btn-info btn-block mt-4"
                  />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
})


const mapDispatchToProps = dispatch => {
  return {
    onRegisterUser: (data, history) => dispatch(registerUser(data, history)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Register));
