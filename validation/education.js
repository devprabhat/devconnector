const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateEducationInput(data) {
    let errors = {};

    data.school = !isEmpty(data.school) ? data.school : '';
    data.degree = !isEmpty(data.degree) ? data.degree : '';
    data.fromdate = !isEmpty(data.fromdate) ? data.fromdate : '';
    data.fieldOfStudy = !isEmpty(data.fieldOfStudy) ? data.fieldOfStudy : '';

    if(validator.isEmpty(data.school)) {
        errors.school = 'School is required';
    }

    if(validator.isEmpty(data.degree)) {
        errors.degree = 'Degree is required';
    }

    if(validator.isEmpty(data.fromdate)) {
        errors.fromdate = 'From date is required';
    }

    if(validator.isEmpty(data.fieldOfStudy)) {
        errors.fieldOfStudy = 'Field of Study is required';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};