const initialState = {
    posts: [],
    post: {},
    loading: false
}

const reducer = (state=initialState, action) {
    switch(action.type) {
        // case GET_ERRORS:
        //     return action.payload;
        default: 
            return state;
    }
}
export default reducer;