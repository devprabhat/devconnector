const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");

//Post model
const Post = require("../../models/Post");

//Profile model
const Profile = require("../../models/Profile");

//Load Post Input validator
const validatePostInput = require("../../validation/post");

// @route   GET api/posts/test
// @desc    Tests post route
// @access  Public
router.get("/test", (req, res) => res.json({ msg: "Posts Works" }));

// @route   GET api/posts
// @desc    Get posts
// @access  Public
router.get("/", (req, res) => {
  Post.find()
    .sort({ date: -1 })
    .then((posts) => res.json(posts))
    .catch((err) => res.status(404).json({ msg: "No post available" }));
});

// @route   GET api/posts/:id
// @desc    Get post
// @access  Public
router.get("/:id", (req, res) => {
  Post.findById(req.params.id)
    .then((post) => res.json(post))
    .catch((err) =>
      res.status(404).json({ msg: "No post found with this id" })
    );
});

// @route   POST api/posts
// @desc    Create post
// @access  Private
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const newPost = new Post({
      text: req.body.text,
      name: req.body.name,
      avatar: req.body.avatar,
      user: req.user.id,
    });

    newPost.save().then((post) => res.json(post));
  }
);

// @route   DELETE api/posts/:id
// @desc    Get post
// @access  Public
router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({user: req.user.id})
      .then(profile =>{
        Post.findById(req.params.id)
          .then(post => {
            if(post.id.toString() !== req.params.id){
              return res.status(401).json({msg: 'You are not allowed to delete this post'})
            }
            //Delete
            post.remove().then(() => res.json({success: true}))
          })
      })
      .catch(err => res.status(404).json({msg: 'Post not found'}))
  }
);

// @route   POST api/posts/like/:id
// @desc    Like post
// @access  Private
router.post(
  "/like/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({user: req.user.id})
      .then(profile =>{
        Post.findById(req.params.id)
          .then(post => {
            if(post.likes.filter(like => like.user.toString() === req.user.id).length > 0){
              return res.status(400).json({msg: 'already liked by this user'})
            }
            //Add the user id to likes array
            post.likes.unshift({user: req.user.id});

            post.save().then(post => res.json(post));
          })
      })
      .catch(err => res.status(404).json({msg: 'Post not found'}))
  }
);

// @route   POST api/posts/unlike/:id
// @desc    UnLike post
// @access  Private
router.post(
  "/unlike/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({user: req.user.id})
      .then(profile =>{
        Post.findById(req.params.id)
          .then(post => {
            if(post.likes.filter(like => like.user.toString() === req.user.id).length === 0){
              return res.status(400).json({msg: 'You have not liked this post'})
            }
            //Get remove index
            const removeIndex = post.likes
              .map(item => item.user.toString()).indexOf(req.user.id);

            //Splice the array
            post.likes.splice(removeIndex, 1); 

            post.save().then(post => res.json(post));
          })
      })
      .catch(err => res.status(404).json({msg: 'Post not found'}))
  }
);

// @route   POST api/posts/comment/:id
// @desc    Comment post
// @access  Private
router.post(
  "/comment/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    Post.findById(req.params.id)
      .then(post => {
        const newComment = {
          text: req.body.comment,
          name: req.body.name,
          avatar: req.body.avatar,
          user: req.user.id
        }
        //Add comment to post
        post.comments.unshift(newComment)

        post.save().then(post => res.json(post));
      })
  .catch(err => res.status(404).json({msg: 'Post not found'}))
  }
);

// @route   POST api/posts/comment/:id/:comment_id
// @desc    Delete comment from post
// @access  Private
router.delete(
  "/comment/:id/:comment_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Post.findById(req.params.id)
    .then(post => {
      if(post.commentc.filter(comment => comment._id.toString() === req.params.comment_id).length === 0){
        return res.status(400).json({msg: 'Comment not fount'})
      }
      //Get remove index
      const removeIndex = post.comments
        .map(item => item._id.toString()).indexOf(req.params.comment_id);

      //Splice the array
      post.comments.splice(removeIndex, 1); 

      post.save().then(post => res.json(post));
    })
      .catch(err => res.status(404).json({msg: 'Post not found'}))
  }
);
module.exports = router;
