import React, { Component } from "react";
import { connect } from "react-redux";
import Moment from "react-moment";
import { deleteEducation } from "../../actions/profileAction";

class Education extends Component {
  deleteExperience = (id) => {
    console.log("Hello");
    this.props.onDeleteExperience(id);
  };
  render() {
    const education = this.props.education.map((edu) => (
      <tr key={edu._id}>
        <td>{edu.school}</td>
        <td>{edu.degree}</td>
        <td>{edu.fieldOfStudy}</td>
        <td>
          <Moment format="DD/MM/YYYY">{edu.fromdate}</Moment> -
          {edu.todate === null ? (
            " Now"
          ) : (
            <Moment format="DD/MM/YYYY">{edu.todate}</Moment>
          )}
        </td>
        <td>
          <button
            className="btn btn-danger"
            onClick={() => this.deleteEducation(edu._id)}
          >
            Delete
          </button>
        </td>
      </tr>
    ));
    return (
      <div className="mb-2">
        <h4 className="mb-4">Education Credentials</h4>
        <table className="table">
          <thead>
            <tr>
              <th>School</th>
              <th>Degree</th>
              <th>Field of Study</th>
              <th>Years</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{education}</tbody>
        </table>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteEducation: (eduId) =>
      dispatch(deleteEducation(eduId)),
  };
};
export default connect(null, mapDispatchToProps)(Education);
