import React, { Component } from "react";
import Moment from "react-moment";
import isEmpty from "../../validation/is-empty";

class ProfileCreds extends Component {
  render() {
    const { profile } = this.props;
    return (
      <div className="row">
        <div className="col-md-6">
          <h3 className="text-center text-info">Experience</h3>
          <ul className="list-group">
            {profile.experience.map((exp) => (
              <li className="list-group-item" key={exp._id}>
                <h4>{exp.company}</h4>
                <Moment format="MMM YYYY">{exp.fromdate}</Moment> -
                {exp.todate === null ? (
                  " Current"
                ) : (
                  <Moment format="MMM YYYY">{exp.todate}</Moment>
                )}
                <p>
                  <strong>Position:</strong> {exp.title}
                </p>
                <p>
                  <strong>Description:</strong> {exp.description}
                </p>
              </li>
            ))}
          </ul>
        </div>
        <div className="col-md-6">
          <h3 className="text-center text-info">Education</h3>
          <ul className="list-group">
            {profile.education.map((edu) => (
              <li className="list-group-item" key={edu._id}>
                <h4>{edu.school}</h4>
                <Moment format="MMM YYYY">{edu.fromdate}</Moment> -
                {edu.todate === null ? (
                  " Current"
                ) : (
                  <Moment format="MMM YYYY">{edu.todate}</Moment>
                )}
                <p>
                  <strong>Degree: </strong>
                  {edu.degree}
                </p>
                <p>
                  <strong>Field Of Study: </strong>
                  {edu.fieldOfStudy}
                </p>
                {!isEmpty(edu.description) ? (
                  <p>
                    <strong>Description:</strong> {edu.description}
                  </p>
                ) : (
                  ""
                )}
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default ProfileCreds;
